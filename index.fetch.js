function Item({episode_id, title, opening_crawl}, active){
  return `<div class="accordion">
    <button data-episode="${episode_id}" class="accordion-btn ${active ? 'active' : ''}">Episode: ${episode_id} ${title}</button>
    <div class="panel" style="${active ? 'display: block;' : ''}">
    <ul class="char-list">Loading...</ul>
      <p>${opening_crawl}</p>
    </div>
  </div>`;
}

function getEpisodes() {
  return fetch('https://swapi.co/api/films/', {
    method: 'GET',
    async: true,
  }).then(resp => resp.json());
}

function getCharacters(urlArray) {
  return Promise.all(
      urlArray.map(url => fetch(url, { method: 'GET' }).then(resp => resp.json()))
  );
}

async function runApp(){
  let container = document.querySelector('.star-wars');
  let response = await getEpisodes();

  response.results.sort((a, b) => a.episode_id > b.episode_id ? 1 :
      a.episode_id < b.episode_id ? -1 :
          0);

  if(container) {
    container.innerHTML = response.results.map((obj, index) => Item(obj, index === 0)).join('');
  }

  container.addEventListener('click', function(event) {
    if(event.target.matches('.accordion-btn')) {
      event.target.classList.toggle('active');

      let panel = event.target.nextElementSibling;
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
    }
  });

  response.results.map(async function({ characters, episode_id }) {
    const chars = await getCharacters(characters);

    const accordion = container.querySelector(`[data-episode="${episode_id}"]`).parentNode;
    const list = accordion.querySelector('.char-list');

    list.innerHTML = chars.map(char => `<li>${char.name}</li>`).join('');

  });
}

runApp();
