/**
 * @param props Настройки для запроса на сервер
 * @param props.method Тип запроса на сервер
 * @param props.url Ссылка куда отправляем запрос
 * @param props.async Синхронно: false, асинхронно: true
 * @param props.data данные на передачу
 * @param props.onSuccess коллбэк функция вызов при успешном запросе
 * @param props.onError коллбэк функция вызов при неуспешном запросе
 * props = properties
 */
function ajax(props) {
  const xhr = new XMLHttpRequest();

  // Настройки поумолчанию + настройки переданные
  props = Object.assign({ method: 'POST', async: true }, props);

  // Обработчик состояния связи с сервером
  xhr.onreadystatechange = function() {
    if(this.readyState === 4 && this.status === 200) {
      props.onSuccess(this.responseText);
    }
  };

  // Перехватываем негативные результаты общения с сервером
  xhr.onload = function() {
    if (xhr.status !== 200) { // analyze HTTP status of the response
      props.onError(new Error(`Error ${xhr.status}: ${xhr.statusText}`));
    }
  };

  xhr.onerror = props.onError;

  xhr.open(props.method, props.url, props.async);
  xhr.send(props.data);
}

function ajaxPromise(props) {
  return new Promise(function(resolve, reject) {
    ajax(Object.assign(props,{
      onSuccess: resolve,
      onError: reject
    }));
  });
}

function Item({episode_id, title, opening_crawl}, active){
  return `<div class="accordion">
    <button data-episode="${episode_id}" class="accordion-btn ${active ? 'active' : ''}">Episode: ${episode_id} ${title}</button>
    <div class="panel" style="${active ? 'display: block;' : ''}">
    <ul class="char-list">Loading...</ul>
      <p>${opening_crawl}</p>
    </div>
  </div>`;
}

function getEpisodes() {
  return ajaxPromise({
    method: 'GET',
    url: 'https://swapi.co/api/films/',
    async: true,
  })
}

function getCharacters(urlArray) {
  return Promise.all(
      urlArray.map(url => ajaxPromise({ method: 'GET', url: url }))
  );
}

async function runApp(){
  let container = document.querySelector('.star-wars');
  let result = await getEpisodes();

  let response = JSON.parse(result);
  response.results.sort((a, b) => a.episode_id > b.episode_id ? 1 :
      a.episode_id < b.episode_id ? -1 :
          0);

  if(container) {
    container.innerHTML = response.results.map((obj, index) => Item(obj, index === 0)).join('');
  }

  container.addEventListener('click', function(event) {
    if(event.target.matches('.accordion-btn')) {
      event.target.classList.toggle('active');

      let panel = event.target.nextElementSibling;
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
    }
  });

  console.log(response);

  response.results.map(async function({ characters, episode_id }) {
    const chars = await getCharacters(characters);

    const accordion = container.querySelector(`[data-episode="${episode_id}"]`).parentNode;
    const list = accordion.querySelector('.char-list');

    list.innerHTML = chars.map(char => `<li>${JSON.parse(char).name}</li>`).join('');

  });

}

runApp();

